// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System;
using System.ComponentModel.DataAnnotations;

namespace Elpida.Web.Backend.Services.Abstractions
{
	/// <summary>
	///     Specifies the the field to must have a non default value.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter)]
	public class NonDefaultValueAttribute : ValidationAttribute
	{
		public NonDefaultValueAttribute()
			: base("The value be the default")
		{
		}

		public override bool IsValid(object? value)
		{
			if (value is null)
			{
				return false;
			}

			var type = value.GetType();

			if (type.IsValueType)
			{
				return !value.Equals(Activator.CreateInstance(type));
			}

			return true;
		}
	}
}