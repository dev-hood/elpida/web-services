// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.ComponentModel.DataAnnotations;

namespace Elpida.Web.Backend.Services.Abstractions
{
	/// <summary>
	///     A request for paged collection of objects.
	/// </summary>
	public sealed class PageRequest
	{
		/// <summary>
		///     The maximum count that a page can have.
		/// </summary>
		public const int MaxCount = 500;

		private readonly int? _count = 10;
		private readonly int? _next = 0;

		/// <summary>
		///     The number of objects to skip.
		/// </summary>
		[Range(0, int.MaxValue)]
		public int? Next
		{
			get => _next;
			init => _next = value ?? _next;
		}

		/// <summary>
		///     The number of objects the page should have.
		/// </summary>
		/// <example>10</example>
		[Range(1, MaxCount)]
		public int? Count
		{
			get => _count;
			init => _count = value ?? _count;
		}
	}
}