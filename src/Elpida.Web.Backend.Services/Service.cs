// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Elpida.Web.Backend.Common.Exceptions;
using Elpida.Web.Backend.Data.Abstractions.Interfaces;
using Elpida.Web.Backend.Data.Abstractions.Models;
using Elpida.Web.Backend.Services.Abstractions;
using Elpida.Web.Backend.Services.Abstractions.Dtos;
using Elpida.Web.Backend.Services.Abstractions.Interfaces;
using Elpida.Web.Backend.Services.Utilities;

namespace Elpida.Web.Backend.Services
{
	public abstract class Service<TDto, TPreview, TModel, TRepository> : IService<TDto, TPreview>
		where TModel : Entity
		where TRepository : IRepository<TModel>
		where TDto : FoundationDto
		where TPreview : FoundationDto
	{
		protected Service(TRepository repository)
		{
			Repository = repository;
		}

		protected TRepository Repository { get; }

		public async Task<TDto> GetSingleAsync(long id, CancellationToken cancellationToken = default)
		{
			var entity = await Repository.GetSingleAsync(id, cancellationToken);

			if (entity == null)
			{
				throw new NotFoundException($"{typeof(TDto).Name} was not found", id);
			}

			return ToDto(entity);
		}

		public Task<PagedResult<TPreview>> GetPagedPreviewsAsync(
			QueryRequest queryRequest,
			CancellationToken cancellationToken = default
		)
		{
			return QueryUtilities.GetPagedProjectionsAsync(
				Repository,
				GetFilterExpressions(),
				queryRequest,
				GetPreviewConstructionExpression(),
				cancellationToken
			);
		}

		public virtual async Task<TDto> GetOrAddAsync(TDto dto, CancellationToken cancellationToken = default)
		{
			var bypassExpression = GetCreationBypassCheckExpression(dto);
			var entity = await ProcessDtoAndCreateModelAsync(dto, cancellationToken);
			if (bypassExpression != null)
			{
				return ToDto(
					await QueryUtilities.GetOrAddSafeAsync(Repository, entity, bypassExpression, cancellationToken)
				);
			}

			entity.Id = 0;
			entity = await Repository.CreateAsync(entity, cancellationToken);
			await Repository.SaveChangesAsync(cancellationToken);
			return ToDto(entity);
		}

		public IEnumerable<FilterExpression> ConstructCustomFilters<T, TR>(Expression<Func<T, TR>> baseExpression)
		{
			return FiltersTransformer.ConstructCustomFilters(baseExpression, GetFilterExpressions());
		}

		public virtual IEnumerable<FilterExpression> GetFilterExpressions()
		{
			yield break;
		}

		protected abstract TDto ToDto(TModel model);

		protected abstract Expression<Func<TModel, TPreview>> GetPreviewConstructionExpression();

		protected abstract Task<TModel> ProcessDtoAndCreateModelAsync(TDto dto, CancellationToken cancellationToken);

		protected virtual Expression<Func<TModel, bool>>? GetCreationBypassCheckExpression(TDto dto)
		{
			return null;
		}
	}
}