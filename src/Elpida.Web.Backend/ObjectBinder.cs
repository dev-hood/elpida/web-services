// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Elpida.Web.Backend
{
	public class ObjectBinder : IModelBinder
	{
		public Task BindModelAsync(ModelBindingContext bindingContext)
		{
			var valueProviderResult = bindingContext.ValueProvider.GetValue(bindingContext.ModelName);
			if (valueProviderResult == ValueProviderResult.None)
			{
				return Task.CompletedTask;
			}

			var str = valueProviderResult.FirstValue;

			if (string.IsNullOrWhiteSpace(str))
			{
				if (bindingContext.ModelType.IsNonNullableReferenceType())
				{
					bindingContext.Result = ModelBindingResult.Failed();
					return Task.CompletedTask;
				}

				bindingContext.Result = ModelBindingResult.Success(null);
				return Task.CompletedTask;
			}

			if (int.TryParse(str, out var i))
			{
				bindingContext.Result = ModelBindingResult.Success(i);
				return Task.CompletedTask;
			}

			if (double.TryParse(str, out var f))
			{
				bindingContext.Result = ModelBindingResult.Success(f);
				return Task.CompletedTask;
			}

			if (bool.TryParse(str, out var b))
			{
				bindingContext.Result = ModelBindingResult.Success(b);
				return Task.CompletedTask;
			}

			bindingContext.Result = ModelBindingResult.Success(str);

			return Task.CompletedTask;
		}
	}
}