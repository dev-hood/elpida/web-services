import {AfterViewInit, Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {BenchmarkResultModel} from '../../../models/result/benchmark-result.model';
import {ActivatedRoute} from '@angular/router';
import {BenchmarkResultsService} from '../../../services/benchmark-results.service';
import {HttpClient} from '@angular/common/http';
import {ImageLinksService} from '../../../services/image-links.service';
import {ValueConverter} from '../../../services/value-converter';
import {TabModel} from '../../../models/tab.model';

@Component({
    selector: 'app-benchmark-result',
    templateUrl: './benchmark-result.component.html',
    styleUrls: ['./benchmark-result.component.css']
})
export class BenchmarkResultComponent implements OnInit, AfterViewInit {

    public result?: BenchmarkResultModel;

    @ViewChild('summaryTitle')
    public summaryTitle?: TemplateRef<any>;

    @ViewChild('summaryContent')
    public summaryContent?: TemplateRef<any>;

    @ViewChild('cpuTitle')
    public cpuTitle?: TemplateRef<any>;

    @ViewChild('cpuContent')
    public cpuContent?: TemplateRef<any>;

    @ViewChild('topologyTitle')
    public topologyTitle?: TemplateRef<any>;

    @ViewChild('topologyContent')
    public topologyContent?: TemplateRef<any>;

    @ViewChild('osTitle')
    public osTitle?: TemplateRef<any>;

    @ViewChild('osContent')
    public osContent?: TemplateRef<any>;

    @ViewChild('elpidaTitle')
    public elpidaTitle?: TemplateRef<any>;

    @ViewChild('elpidaContent')
    public elpidaContent?: TemplateRef<any>;

    @ViewChild('taskResultsTitle')
    public taskResultsTitle?: TemplateRef<any>;

    @ViewChild('taskResultsContent')
    public taskResultsContent?: TemplateRef<any>;

    public tabs: TabModel[] = [];

    public constructor(
        private readonly resultsService: BenchmarkResultsService,
        private readonly http: HttpClient,
        public readonly imageLinksService: ImageLinksService,
        public readonly valueConverter: ValueConverter,
        private readonly route: ActivatedRoute) {

    }

    public async ngOnInit(): Promise<void> {
        this.route.params.subscribe(async p => {
            this.result = await this.resultsService.getSingle(p['id']);
            this.result.timeStamp = new Date(this.result.timeStamp.toString());
        });
    }

    public ngAfterViewInit(): void {
        this.tabs = [
            // @ts-ignore
            new TabModel(this.summaryTitle, this.summaryContent),
            // @ts-ignore
            new TabModel(this.cpuTitle, this.cpuContent),
            // @ts-ignore
            new TabModel(this.topologyTitle, this.topologyContent),
            // @ts-ignore
            new TabModel(this.osTitle, this.osContent),
            // @ts-ignore
            new TabModel(this.elpidaTitle, this.elpidaContent),
            // @ts-ignore
            new TabModel(this.taskResultsTitle, this.taskResultsContent),
        ];
    }
}
