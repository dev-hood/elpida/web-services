import {Component} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

@Component({
    selector: 'app-nav-menu',
    templateUrl: './nav-menu.component.html',
    styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent {
    public isExpanded: boolean = false;

    public constructor(private router: Router) {
        router.events.subscribe(ev => {
            if (ev instanceof NavigationEnd) {
                this.isExpanded = false;
            }
        });
    }

    public toggle(): void {
        this.isExpanded = !this.isExpanded;
    }
}
