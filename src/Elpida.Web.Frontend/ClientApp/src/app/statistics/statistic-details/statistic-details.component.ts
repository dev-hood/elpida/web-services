import {Component, OnInit} from '@angular/core';
import {BenchmarkStatisticsService} from '../../../services/benchmark-statistics.service';
import {ActivatedRoute} from '@angular/router';
import {BenchmarkStatisticsModel} from '../../../models/benchmark-statistics/benchmark-statistics.model';
import {ValueConverter} from '../../../services/value-converter';

import * as shape from 'd3-shape';
import {FrequencyClassModel} from '../../../models/benchmark-statistics/frequency-class.model';
import {ImageLinksService} from '../../../services/image-links.service';

@Component({
    selector: 'app-statistic-details',
    templateUrl: './statistic-details.component.html',
    styleUrls: ['./statistic-details.component.css']
})
export class StatisticDetailsComponent implements OnInit {

    public data?: object[];

    public curve: any = shape.curveBumpX;

    public score?: string;

    public statistics?: BenchmarkStatisticsModel;

    public colors: any = {
        domain: ['#7c80d7']
    };

    public constructor(private readonly statisticsService: BenchmarkStatisticsService,
                       public readonly imageLinksService: ImageLinksService,
                       private readonly route: ActivatedRoute,
                       public readonly valueConverter: ValueConverter) {
    }

    public yTickFormatter = (x: any) => this.valueConverter.toStringSI(x, '');

    public ngOnInit(): void {
        this.route.params.subscribe(async p => {
            this.statistics = await this.statisticsService.getSingle(p['id']);
            if (this.statistics !== undefined) {
                const unit = this.statistics.benchmark.scoreSpecification.unit;
                this.score = this.valueConverter.toStringSI(this.statistics.mean, unit);
                this.data = [
                    {
                        'name': this.statistics.benchmark.name,
                        'series': this.statistics.classes.map(x => {
                            return {
                                'name': this.getClassString(x, unit),
                                'value': x.count
                            };
                        })
                    }
                ];
            }
        });
    }

    public formatNumberSI(arg: number): string {
        return this.valueConverter.toStringSI(arg, null);  // Use static because charts call this before construction????
    }

    private getClassString(cls: FrequencyClassModel, unit: string): string {
        return `${this.valueConverter.toStringSI(cls.low, unit, 2)} - ${this.valueConverter.toStringSI(cls.high, unit, 2)}`;
    }
}
