import {Component, ContentChild, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BenchmarkStatisticsService} from '../../../services/benchmark-statistics.service';
import {BenchmarkService} from '../../../services/benchmark.service';
import {BenchmarkModel} from '../../../models/benchmark/benchmark.model';
import {NumberFilterModel} from '../../../models/filters/number-filter.model';
import {BenchmarkStatisticsPreviewModel} from '../../../models/benchmark-statistics/benchmark-statistics-preview.model';
import {ValueConverter} from '../../../services/value-converter';
import {BenchmarkComparison} from '../../../models/benchmark/benchmark-score-specification.model';
import {ComparisonModel} from '../../../models/comparison.model';
import {ImageLinksService} from '../../../services/image-links.service';
import {QueryModel} from '../../../models/query.model';
import {StringFilterModel} from '../../../models/filters/string-filter.model';
import {PagedCollectionService} from '../../../services/paged-collection.service';
import {PagedCollectionModel} from '../../../models/paged-collection.model';

@Component({
    selector: 'app-top-cpus-by-benchmark',
    templateUrl: './top-cpus-by-benchmark.component.html',
    styleUrls: ['./top-cpus-by-benchmark.component.css']
})
export class TopCpusByBenchmarkComponent implements OnInit {

    public benchmark?: BenchmarkModel;

    public orderBy?: string;

    public chartXAxisLabel: string = 'N/A';

    public chartData: any;

    public colourScheme: any = {domain: ['#898EE2FF']};

    @ContentChild('itemTemplate')
    public itemTemplate: TemplateRef<BenchmarkStatisticsPreviewModel> | null = null;

    public collectionModel: PagedCollectionModel<BenchmarkStatisticsPreviewModel> = new PagedCollectionModel<BenchmarkStatisticsPreviewModel>();

    public constructor(private readonly route: ActivatedRoute,
                       private readonly benchmarkService: BenchmarkService,
                       public readonly imageLinksService: ImageLinksService,
                       public readonly statisticsService: BenchmarkStatisticsService,
                       public readonly valueConverter: ValueConverter,
                       private readonly collectionService: PagedCollectionService) {
    }

    public calculateActualStatisticValue(preview: BenchmarkStatisticsPreviewModel): string {
        return this.valueConverter.toStringSI(preview.mean, preview.benchmarkScoreUnit);
    }

    public toItem(context: any): BenchmarkStatisticsPreviewModel {
        return context as BenchmarkStatisticsPreviewModel;
    }

    public async ngOnInit(): Promise<void> {
        this.collectionModel.collectionChanged.subscribe(s => this.buildChartData(s));
        this.route.params.subscribe(async p => {
            const benchmarkId = p['id'];
            if (benchmarkId !== undefined) {
                this.benchmark = await this.benchmarkService.getSingle(benchmarkId);
                this.collectionModel.lockedFilters = [
                    new NumberFilterModel('', 'benchmarkId', false, ComparisonModel.equals(), undefined, this.benchmark.id)
                ];
                this.collectionModel.lockedOrderBy = this.orderBy = this.statisticsService.createBenchmarkScoreMeanFilter().internalName;
                this.collectionModel.lockedDescending = this.benchmark.scoreSpecification.comparison === BenchmarkComparison.Greater;
                await this.collectionService.initialize(this.collectionModel, this.statisticsService);
            }
        });
    }

    public getComparisonDescription(): string {
        // @ts-ignore
        return this.benchmark.scoreSpecification.comparison === BenchmarkComparison.Lower ?
            'Lower is better'
            : 'Higher is better';
    }

    public buildChartData(collection: BenchmarkStatisticsPreviewModel[]): void {
        if (collection !== undefined) {

            if (collection.length === 0) {
                this.chartXAxisLabel = this.benchmark?.scoreSpecification.unit ?? 'N/A';
                this.chartData = [];
                return;
            }

            const value = this.valueConverter.getValueScaleSI(collection[0].mean);
            // @ts-ignore
            this.chartXAxisLabel = value.suffix + this.benchmark.scoreSpecification.unit;
            this.chartData = collection.map(i => {
                return {
                    'name': i.cpuModelName,
                    'value': i.mean / value.value
                };
            });
        }
    }

    public async onQuerySubmit(query: QueryModel): Promise<void> {
        await this.collectionService.submitQuery(this.collectionModel, query, 0);
    }

    public async onPageChanged(page: number): Promise<void> {
        await this.collectionService.changePage(this.collectionModel, page);
    }

    public async onReverted(): Promise<void> {
        if (this.benchmarkService !== undefined) {
            await this.collectionService.revert(this.collectionModel, this.statisticsService);
        }
    }

    public async onSearched(filter: StringFilterModel): Promise<void> {
        if (this.benchmarkService !== undefined) {
            await this.collectionService.search(this.collectionModel, filter);
        }
    }

}
