import {Component, ContentChild, Input, OnInit, TemplateRef} from '@angular/core';
import {ICollectionService} from '../../../services/interfaces/icollection-service';
import {QueryModel} from '../../../models/query.model';
import {ActivatedRoute, Router} from '@angular/router';
import {QueryModelService} from '../../../services/query-model.service';
import {DtoService} from '../../../services/dto.service';
import {StringFilterModel} from '../../../models/filters/string-filter.model';
import {PagedCollectionService} from '../../../services/paged-collection.service';
import {PagedCollectionModel} from '../../../models/paged-collection.model';

@Component({
    selector: 'app-collection-mediator',
    templateUrl: './collection-mediator.component.html',
    styleUrls: ['./collection-mediator.component.css']
})
export class CollectionMediatorComponent<TModel, TPreview> implements OnInit {

    @Input()
    public service?: ICollectionService<TModel, TPreview>;

    @ContentChild('itemTemplate')
    public itemTemplate: TemplateRef<TPreview> | null = null;

    public collectionModel: PagedCollectionModel<TPreview> = new PagedCollectionModel<TPreview>();

    public constructor(private readonly route: ActivatedRoute,
                       private readonly router: Router,
                       private readonly queryModelService: QueryModelService,
                       private readonly dtoService: DtoService,
                       private readonly collectionService: PagedCollectionService) {
    }

    public ngOnInit(): void {
        if (this.service !== undefined) {
            this.collectionService.initialize(this.collectionModel, this.service);
        }
    }

    public async onQuerySubmit(query: QueryModel): Promise<void> {
        await this.collectionService.submitQuery(this.collectionModel, query, 0);
    }

    public async onPageChanged(page: number): Promise<void> {
        await this.collectionService.changePage(this.collectionModel, page);
    }

    public async onReverted(): Promise<void> {
        if (this.service !== undefined) {
            await this.collectionService.revert(this.collectionModel, this.service);
        }
    }

    public async onSearched(filter: StringFilterModel): Promise<void> {
        if (this.service !== undefined) {
            await this.collectionService.search(this.collectionModel, filter);
        }
    }
}
