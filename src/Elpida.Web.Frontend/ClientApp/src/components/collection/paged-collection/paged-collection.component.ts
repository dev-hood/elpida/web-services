import {Component, EventEmitter, Input, OnInit, Output, TemplateRef} from '@angular/core';
import {QueryModel} from '../../../models/query.model';
import {ValueConverter} from '../../../services/value-converter';
import {StringFilterModel} from '../../../models/filters/string-filter.model';
import {PagedCollectionModel} from '../../../models/paged-collection.model';

@Component({
    selector: 'app-paged-collection',
    templateUrl: './paged-collection.component.html',
    styleUrls: ['./paged-collection.component.css']
})
export class PagedCollectionComponent<T> implements OnInit {
    public advancedShown: boolean = false;

    public filtersPanelShown: boolean = false;

    @Input()
    public showFilters: boolean = true;

    @Input()
    public model: PagedCollectionModel<T> = new PagedCollectionModel<T>();

    @Output()
    public pageChanged: EventEmitter<number> = new EventEmitter<number>();

    @Output()
    public searched: EventEmitter<StringFilterModel> = new EventEmitter<StringFilterModel>();

    @Output()
    public querySubmitted: EventEmitter<QueryModel> = new EventEmitter<QueryModel>();

    @Output()
    public reverted: EventEmitter<any> = new EventEmitter<any>();

    @Input()
    public itemTemplate: TemplateRef<any> | null = null;

    public constructor(public readonly valueConverter: ValueConverter) {
    }

    public onSimpleClicked(): void {
        this.showSimpleQuery();
    }

    public onAdvancedClicked(): void {
        this.showAdvancedQuery();
    }

    public onFiltersButtonClick(): void {
        this.filtersPanelShown = !this.filtersPanelShown;
    }

    public onFiltersSubmitted(query: QueryModel): void {
        this.filtersPanelShown = false;
        this.querySubmitted.emit(query);
    }

    public revertPage(): void {
        this.reverted.emit();
    }

    public onSearch(ev: KeyboardEvent): void {
        if (this.model.searchFilter !== undefined && ev.key === 'Enter') {
            if (this.model.searchFilter.isSet()) {
                this.searched.emit(this.model.searchFilter);
            }
        }
    }

    public onPageChange(page: number): void {
        if (this.model.currentPage !== page) {    // avoid multiple API Calls from initialisation
            this.pageChanged.emit(page);
        }
    }

    public ngOnInit(): void {
    }
    
    private showAdvancedQuery(): void {
        this.advancedShown = true;
    }

    private showSimpleQuery(): void {
        this.advancedShown = false;
    }
}

