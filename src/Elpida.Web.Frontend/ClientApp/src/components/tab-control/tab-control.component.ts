import {Component, Input, OnInit} from '@angular/core';
import {TabModel} from '../../models/tab.model';

@Component({
    selector: 'app-tab-control',
    templateUrl: './tab-control.component.html',
    styleUrls: ['./tab-control.component.css']
})
export class TabControlComponent implements OnInit {

    @Input()
    public tabs: TabModel[] = [];

    public activeTab?: TabModel;

    public ngOnInit(): void {
        if (this.tabs !== undefined && this.tabs.length > 0) {
            this.activeTab = this.tabs[0];
        }
    }
}
