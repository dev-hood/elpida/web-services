export class PageDto {
    public constructor(
        public readonly next: number,
        public readonly count: number
    ) {
    }
}
