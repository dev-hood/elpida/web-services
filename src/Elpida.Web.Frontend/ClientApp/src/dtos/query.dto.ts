import {PageDto} from './page.dto';
import {FilterDto} from './filter.dto';

export class QueryDto {

    public constructor(public readonly pageRequest: PageDto,
                public readonly orderBy?: string | null,
                public readonly descending?: boolean,
                public readonly filters?: FilterDto[]) {
    }
}
