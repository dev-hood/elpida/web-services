export const environment = {
    production: true,
    apiUrl: 'https://api.elpida.dev/api/v1/',
    sourceBranch: 'master'
};
