export const environment = {
    production: false,
    apiUrl: 'https://staging.api.elpida.dev/api/v1/',
    sourceBranch: 'staging'
};
