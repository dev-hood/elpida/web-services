export class FrequencyClassModel {
    public constructor(
        public readonly low: number,
        public readonly high: number,
        public readonly count: number
    ) {
    }
}
