export class BenchmarkPreviewModel {
    public constructor(
        public readonly id: number,
        public readonly uuid: string,
        public readonly name: string
    ) {
    }
}
