export class CompilerInfoModel {
    public constructor(
        public readonly name: string,
        public readonly version: string
    ) {
    }
}
