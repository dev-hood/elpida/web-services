import {ValueFilterModel} from '../value-filter.model';
import {ComparisonModel} from '../comparison.model';
import {FilterModel} from '../filter.model';

export class StringFilterModel extends ValueFilterModel<string> {

    protected defaultValue: string = '';

    public constructor(title: string,
                       internalName: string,
                       allowComparisonChange: boolean = true,
                       comparison: ComparisonModel = ComparisonModel.contains(),
                       value?: string) {
        super(title,
            internalName,
            StringFilterModel.stringComparisons,
            allowComparisonChange,
            comparison,
            value
        );
    }

    public clone(): FilterModel {
        return new StringFilterModel(this.title, this.internalName, this.allowComparisonChange, this.comparison, this.value);
    }

    public trySetValue(value: any): boolean {
        this.value = value.toString();
        return true;
    }
}
