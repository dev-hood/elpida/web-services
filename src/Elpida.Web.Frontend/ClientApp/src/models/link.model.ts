export class LinkModel {
    public constructor(public readonly name: string,
                public readonly url: string) {
    }
}
