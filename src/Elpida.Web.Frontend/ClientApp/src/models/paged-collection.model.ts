import {QueryModel} from './query.model';
import {StringFilterModel} from './filters/string-filter.model';
import {FilterModel} from './filter.model';
import {EventEmitter} from '@angular/core';

export class PagedCollectionModel<T> {
    public collectionChanged: EventEmitter<T[]> = new EventEmitter<T[]>();
    public collection?: T[];
    public simpleQuery?: QueryModel;
    public advancedQuery?: QueryModel;
    public availableFilters: FilterModel[] = [];
    public currentPage: number = 0;
    public pageCount: number = 0;
    public totalItemCount: number = 0;
    public searchFilter?: StringFilterModel;
    public itemsPerPage: number = 10;
    public lockedFilters?: FilterModel[];
    public lockedOrderBy?: string;
    public lockedDescending?: boolean;
}
