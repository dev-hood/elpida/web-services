import {FilterModel} from './filter.model';

export class QueryModel {
    public constructor(public filters: FilterModel[],
                       public orderBy?: string | null,
                       public descending: boolean = false) {
    }

    public clone(): QueryModel {
        return new QueryModel(this.filters.map(f => f.clone()), this.orderBy, this.descending);
    }
}
