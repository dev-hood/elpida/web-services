export class MemoryModel {
    public constructor(
        public readonly totalSize: number,
        public readonly pageSize: number,
    ) {
    }
}
