import {FilterModel} from './filter.model';
import {ComparisonModel} from './comparison.model';

export abstract class ValueFilterModel<T> extends FilterModel {

    public static allComparisons: ComparisonModel[] = [
        ComparisonModel.equals(),
        ComparisonModel.notEqual(),
        ComparisonModel.contains(),
        ComparisonModel.notContain(),
        ComparisonModel.greater(),
        ComparisonModel.greaterEqual(),
        ComparisonModel.less(),
        ComparisonModel.lessEqual(),
    ];

    public static numericComparisons: ComparisonModel[] = [
        ComparisonModel.equals(),
        ComparisonModel.notEqual(),
        ComparisonModel.greater(),
        ComparisonModel.greaterEqual(),
        ComparisonModel.less(),
        ComparisonModel.lessEqual(),
    ];

    public static stringComparisons: ComparisonModel[] = [
        ComparisonModel.equals(),
        ComparisonModel.notEqual(),
        ComparisonModel.contains(),
        ComparisonModel.notContain()
    ];

    protected abstract defaultValue: T;

    private readonly defaultComparison?: ComparisonModel;
    private readonly originalValue?: T;

    /**
     * Creates te base value filter model.
     * @param title The title of the filter to show in view
     * @param internalName The internal name in use in the backend.
     * @param validComparisons The valid comparisons for this filter type.
     * @param allowComparisonChange Whether the view should allow changing the comparison.
     * @param comparison The selected comparison for this filter.
     * @param value The filter value.
     * @protected
     */
    protected constructor(title: string,
                          internalName: string,
                          public readonly validComparisons: ComparisonModel[] = ValueFilterModel.allComparisons,
                          public allowComparisonChange: boolean,
                          public comparison?: ComparisonModel,
                          public value?: T) {
        super(title, internalName);
        this.originalValue = value;
        this.defaultComparison = comparison;
    }


    public abstract trySetValue(value: any): boolean;

    public isSet(): boolean {
        return this.comparison !== null
            && this.value !== undefined
            && this.value !== this.defaultValue
            && this.value !== null;
    }

    public reset(): void {
        this.value = this.originalValue;
        this.comparison = this.defaultComparison;
    }
}
