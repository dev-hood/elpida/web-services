import {Injectable} from '@angular/core';
import {DownloadInstanceModel} from '../models/download-instance.model';
import {LinkModel} from '../models/link.model';
import {environment} from '../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LinksService {

    public nativeRepoLink: string = 'https://gitlab.com/dev-hood/elpida/elpida';
    public webServicesRepoLink: string = 'https://gitlab.com/dev-hood/elpida/web-services';

    public readonly binariesLinks: DownloadInstanceModel[] = [
        new DownloadInstanceModel('Windows',
            [
                new LinkModel(
                    'Beta x86-64 (.zip)',
                    `https://gitlab.com/dev-hood/elpida/elpida/-/jobs/artifacts/${environment.sourceBranch}/raw/Elpida-latest-x86_64.zip?job=deploy:windows`
                )
            ],
            [
                new LinkModel(
                    'sha256sum',
                    `https://gitlab.com/dev-hood/elpida/elpida/-/jobs/artifacts/${environment.sourceBranch}/raw/Elpida-latest-x86_64.zip.SHA256SUMS?job=deploy:windows`
                )
            ]
        ),
        new DownloadInstanceModel('Linux',
            [
                new LinkModel(
                    'Beta x86-64 (.AppImage)',
                    `https://gitlab.com/dev-hood/elpida/elpida/-/jobs/artifacts/${environment.sourceBranch}/raw/Elpida-latest-x86_64.AppImage?job=deploy:linux`
                )
            ],
            [
                new LinkModel(
                    'sha256sum',
                    `https://gitlab.com/dev-hood/elpida/elpida/-/jobs/artifacts/${environment.sourceBranch}/raw/Elpida-latest-x86_64.AppImage.SHA256SUMS?job=deploy:linux`
                )
            ]
        )
    ];

    public readonly sourcesLinks: DownloadInstanceModel[] = [
        new DownloadInstanceModel('Elpida source code',
            [
                new LinkModel(
                    'Beta source code (.tar.gz)',
                    `https://gitlab.com/dev-hood/elpida/elpida/-/archive/${environment.sourceBranch}/elpida-${environment.sourceBranch}.tar.gz`
                )
            ],
            [
                new LinkModel(
                    'Git repository',
                    this.nativeRepoLink
                )
            ]
        ),
        new DownloadInstanceModel('Web services source code',
            [
                new LinkModel(
                    'Beta source code (.tar.gz)',
                    `https://gitlab.com/dev-hood/elpida/web-services/-/archive/${environment.sourceBranch}/web-services-${environment.sourceBranch}.tar.gz`,
                )
            ],
            [
                new LinkModel(
                    'Git repository',
                    this.webServicesRepoLink
                )
            ]
        )];
}
