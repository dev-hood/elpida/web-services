import { TestBed } from '@angular/core/testing';

import { PagedCollectionService } from './paged-collection.service';

describe('PagedCollectionService', () => {
  let service: PagedCollectionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PagedCollectionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
