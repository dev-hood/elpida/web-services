import {Injectable} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {QueryModelService} from './query-model.service';
import {DtoService} from './dto.service';
import {PagedCollectionModel} from '../models/paged-collection.model';
import {ICollectionService} from './interfaces/icollection-service';
import {QueryModel} from '../models/query.model';
import {StringFilterModel} from '../models/filters/string-filter.model';
import {C, F} from '@angular/cdk/keycodes';

@Injectable({
    providedIn: 'root'
})
export class PagedCollectionService {


    public constructor(private readonly route: ActivatedRoute,
                       private readonly router: Router,
                       private readonly queryModelService: QueryModelService,
                       private readonly dtoService: DtoService) {
    }

    private static getActualQuery(model: PagedCollectionModel<any>, query: QueryModel): QueryModel {
        return new QueryModel(
            query.filters.concat(model.lockedFilters ?? []),
            model.lockedOrderBy ?? query.orderBy,
            model.lockedDescending ?? query.descending);
    }

    public initialize(model: PagedCollectionModel<any>, service: ICollectionService<any, any>): void {
        model.simpleQuery = service.createSimpleQuery();
        model.advancedQuery = service.createAdvancedQuery();
        model.availableFilters = service.createAdvancedQuery().filters;
        model.searchFilter = service.createSearchFilter();
        const filters = model.advancedQuery.filters;
        this.route.queryParams.subscribe(async p => {
            const tuple = this.queryModelService.createModelFromParams(p, filters);
            model.advancedQuery = tuple.query;
            model.currentPage = tuple.page;
            const actualQuery = PagedCollectionService.getActualQuery(model, model.advancedQuery);
            const result = await service.getPreviews(
                this.dtoService.createQueryDto(actualQuery, model.currentPage, model.itemsPerPage)
            );
            if (result !== undefined) {
                model.totalItemCount = result.totalCount;
                model.pageCount = Math.ceil(result.totalCount / model.itemsPerPage);
                model.collection = result.items;
                model.collectionChanged.emit(result.items);
            }
        });

    }

    public async submitQuery(model: PagedCollectionModel<any>, query: QueryModel, page: number): Promise<void> {
        await this.router.navigate([], {
            relativeTo: this.route,
            queryParams: this.queryModelService.createHttpQueryObjectFromModel(query, page)
        });
    }

    public async changePage(model: PagedCollectionModel<any>, page: number): Promise<void> {
        if (model.advancedQuery !== undefined) {
            await this.submitQuery(model, model.advancedQuery, page);
        }
    }

    public revert(model: PagedCollectionModel<any>, service: ICollectionService<any, any>): Promise<void> {
        return this.submitQuery(model, service.createSimpleQuery(), 0);
    }

    public search(model: PagedCollectionModel<any>, filter: StringFilterModel): Promise<void> {
        return this.submitQuery(model, new QueryModel([filter]), 0);
    }

}
