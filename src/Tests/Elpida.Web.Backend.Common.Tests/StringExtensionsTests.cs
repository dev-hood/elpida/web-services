// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Security.Cryptography;
using System.Text;
using Elpida.Web.Backend.Common.Extensions;
using NUnit.Framework;

namespace Elpida.Web.Backend.Common.Tests
{
	[TestFixture]
	public class StringExtensionsTests
	{
		[Test]
		public void Success()
		{
			const string str = "LOL";
			using var md5 = MD5.Create();

			var expected = md5.ComputeHash(Encoding.UTF8.GetBytes(str));

			Assert.AreEqual(expected.ToHexString(), str.ToHashString());
		}
	}
}