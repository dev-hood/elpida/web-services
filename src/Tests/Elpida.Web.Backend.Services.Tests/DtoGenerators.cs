// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using Elpida.Web.Backend.Common;
using Elpida.Web.Backend.Services.Abstractions;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Benchmark;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Cpu;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Elpida;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Os;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Result;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Result.Batch;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Statistics;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Task;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Topology;

namespace Elpida.Web.Backend.Services.Tests
{
	public static class DtoGenerators
	{
		public static BenchmarkDto NewBenchmark(long? id = null)
		{
			return new BenchmarkDto
			{
				Id = id ?? NewId(),
				Uuid = Guid.NewGuid(),
				Name = "Test Benchmark",
				ScoreSpecification = NewBenchmarkScoreSpecification(),
				Tasks = new[] { BenchmarkTaskDto() },
			};
		}

		public static BenchmarkPreviewDto NewBenchmarkPreview(long? id = null)
		{
			return new BenchmarkPreviewDto
			{
				Id = id ?? NewId(),
				Uuid = Guid.NewGuid(),
				Name = "Test Benchmark",
			};
		}

		public static TaskDto NewTask(long? id = null, Guid? uuid = null)
		{
			return new TaskDto
			{
				Id = id ?? NewId(),
				Uuid = uuid ?? Guid.NewGuid(),
				Name = "Test Task",
				Description = "Test description",
				Result = NewResultSpecification(),
				Input = NewDataSpecification(),
				Output = NewDataSpecification(),
			};
		}

		public static BenchmarkTaskDto BenchmarkTaskDto()
		{
			return new BenchmarkTaskDto
			{
				Uuid = Guid.NewGuid(),
				Task = NewTask(),
				CanBeDisabled = true,
				CanBeMultiThreaded = true,
				IterationsToRun = 5,
				IsCountedOnResults = true,
			};
		}

		public static QueryRequest NewQueryRequest()
		{
			return new QueryRequest(
				NewPage(),
				new[] { new FilterInstance { Name = "data", Value = "test", Comparison = "equal" } },
				"data",
				true
			);
		}

		public static BenchmarkResultPreviewDto NewBenchmarkResultPreview(long? id = null)
		{
			return new BenchmarkResultPreviewDto
			{
				Id = id ?? NewId(),
				BenchmarkUuid = Guid.NewGuid(),
				TimeStamp = DateTime.UtcNow,
				BenchmarkName = "Test benchmark",
				OsName = "Gentoo",
				CpuVendor = "ARM",
				CpuModelName = "Cortex A7",
				BenchmarkScoreUnit = "b/s",
				Score = 46,
			};
		}

		public static BenchmarkResultDto NewBenchmarkResult(long? id = null)
		{
			return new BenchmarkResultDto
			{
				Id = id ?? NewId(),
				TimeStamp = DateTime.UtcNow,
				Uuid = Guid.NewGuid(),
				Name = "Test Benchmark",
				Affinity = new long[] { 1, 2 },
				ElpidaVersion = NewElpida(),
				System = NewSystem(),
				Score = 654,
				ScoreSpecification = NewBenchmarkScoreSpecification(),
				TaskResults = new[] { NewTaskResult() },
			};
		}

		public static TaskResultDto NewTaskResult(long? id = null)
		{
			return new TaskResultDto
			{
				Id = id ?? NewId(),
				CpuId = 2,
				TopologyId = 6,
				BenchmarkResultId = 7,
				Uuid = Guid.NewGuid(),
				Name = "Test task",
				Description = "Test description",
				Result = NewResultSpecification(),
				Input = NewDataSpecification(),
				Output = NewDataSpecification(),
				Value = 4564,
				Time = 13,
				InputSize = 6546,
				Statistics = NewTaskRunStatistics(),
			};
		}

		public static TaskRunStatisticsDto NewTaskRunStatistics()
		{
			return new TaskRunStatisticsDto
			{
				Max = 56,
				Min = 2,
				Mean = 6,
				Tau = 54,
				SampleSize = 4,
				StandardDeviation = 4,
				MarginOfError = 6,
			};
		}

		public static DataSpecificationDto NewDataSpecification()
		{
			return new DataSpecificationDto
			{
				Name = "Test Data",
				Description = "Test description",
				Unit = "bytes",
				RequiredProperties = new[] { "Size" },
			};
		}

		public static ResultSpecificationDto NewResultSpecification()
		{
			return new ResultSpecificationDto
			{
				Name = "Result",
				Description = "Result description",
				Unit = "b/s",
				Aggregation = AggregationType.Average,
				Type = ResultType.Throughput,
			};
		}

		public static BenchmarkScoreSpecificationDto NewBenchmarkScoreSpecification()
		{
			return new BenchmarkScoreSpecificationDto
			{
				Unit = "b/s",
				Comparison = ValueComparison.Greater,
			};
		}

		public static PageRequest NewPage()
		{
			return new PageRequest { Next = 10, Count = 10 };
		}

		public static VersionDto NewVersion()
		{
			return new VersionDto
			{
				Major = 5,
				Minor = 1,
				Revision = 2,
				Build = 0,
			};
		}

		public static CompilerDto NewCompiler()
		{
			return new CompilerDto
			{
				Name = "GCC",
				Version = "10.0",
			};
		}

		public static ElpidaVersionDto NewElpida(long? id = null)
		{
			return new ElpidaVersionDto
			{
				Id = id ?? NewId(),
				Version = NewVersion(),
				Compiler = NewCompiler(),
			};
		}

		public static MemoryDto NewMemory()
		{
			return new MemoryDto
			{
				TotalSize = 465454,
				PageSize = 4096,
			};
		}

		public static TimingDto NewTiming()
		{
			return new TimingDto
			{
				JoinOverhead = 123,
				LockOverhead = 456,
				LoopOverhead = 789,
				NotifyOverhead = 1569,
				NowOverhead = 8799,
				SleepOverhead = 132,
				TargetTime = 64,
				WakeupOverhead = 789,
				UnstableTiming = true,
			};
		}

		public static SystemDto NewSystem()
		{
			return new SystemDto
			{
				Cpu = NewCpu(),
				Os = NewOs(),
				Topology = NewTopology(),
				Memory = NewMemory(),
				Timing = NewTiming(),
			};
		}

		public static CpuNodeDto NewRootCpuNode()
		{
			return new CpuNodeDto
			{
				NodeType = ProcessorNodeType.Machine,
				Name = "Machine",
				OsIndex = 0,
				Value = 0,
				Children = Enumerable.Repeat(NewChildNode(), 4).ToArray(),
				MemoryChildren = Enumerable.Repeat(NewChildNode(), 4).ToArray(),
			};
		}

		public static CpuNodeDto NewChildNode()
		{
			return new CpuNodeDto
			{
				NodeType = ProcessorNodeType.Core,
				Name = "Core",
				OsIndex = 0,
				Value = 0,
				Children = null,
				MemoryChildren = null,
			};
		}

		public static TopologyDto NewTopology(long? id = null)
		{
			return new TopologyDto
			{
				Id = id ?? NewId(),
				CpuId = 3,
				CpuVendor = "x86_64",
				CpuModelName = "Ryzen TR",
				TotalLogicalCores = 128,
				TotalPhysicalCores = 64,
				TotalPackages = 4,
				TotalNumaNodes = 1,
				Root = NewRootCpuNode(),
			};
		}

		public static TopologyPreviewDto NewTopologyPreviewDto(long? id = null)
		{
			return new TopologyPreviewDto
			{
				Id = id ?? NewId(),
				CpuId = 2,
				CpuVendor = "ARM",
				CpuModelName = "Exynos",
				TotalLogicalCores = 16,
				TotalPhysicalCores = 8,
				TotalPackages = 2,
				TotalNumaNodes = 2,
				Hash = "sdfdsf",
			};
		}

		public static OperatingSystemDto NewOs(long? id = null)
		{
			return new OperatingSystemDto
			{
				Id = id ?? NewId(),
				Category = "Linux",
				Name = "KDE Neon",
				Version = "21.1",
			};
		}

		public static CpuCacheDto NewCache()
		{
			return new CpuCacheDto
			{
				Name = "L5",
				Associativity = "Satoko Hojo",
				Size = 4654641,
				LineSize = 1320,
			};
		}

		public static CpuDto NewCpu(long? id = null)
		{
			return new CpuDto
			{
				Id = id ?? NewId(),
				Architecture = "ARM",
				Vendor = "Samsung",
				ModelName = "Exynos",
				Frequency = 52654684351,
				Smt = true,
				AdditionalInfo = new Dictionary<string, string>(),
				Caches = Enumerable.Repeat(NewCache(), 4).ToArray(),
				Features = Enumerable.Repeat("LOL", 4).ToArray(),
			};
		}

		public static CpuPreviewDto NewCpuPreview(long? id = null)
		{
			return new CpuPreviewDto
			{
				Id = id ?? NewId(),
				Vendor = "Samsung",
				ModelName = "Exynos",
			};
		}

		public static BenchmarkStatisticsDto NewBenchmarkStatistic(long? id = null)
		{
			return new BenchmarkStatisticsDto
			{
				Id = id ?? NewId(),
				Cpu = NewCpu(),
				Benchmark = NewBenchmark(),
				Max = 52,
				Min = 46,
				Mean = 59,
				Tau = 12,
				SampleSize = 6,
				StandardDeviation = 5,
				MarginOfError = 8,
				Classes = new[] { NewFrequencyClass() },
			};
		}

		public static BenchmarkStatisticsPreviewDto NewBenchmarkStatisticsPreview(long? id = null)
		{
			return new BenchmarkStatisticsPreviewDto
			{
				Id = id ?? NewId(),
				CpuVendor = "ARM",
				CpuModelName = "Cortex A7",
				BenchmarkUuid = Guid.NewGuid(),
				BenchmarkName = "Test Benchmark",
				BenchmarkScoreUnit = "b/s",
				Mean = 65,
				SampleSize = 46,
				Comparison = ValueComparison.Greater,
			};
		}

		public static FrequencyClassDto NewFrequencyClass()
		{
			return new FrequencyClassDto
			{
				Count = 56,
				High = 45,
				Low = 6,
			};
		}

		public static TaskResultSlimDto NewTaskResultSlim()
		{
			return new TaskResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Time = 46,
				Value = 123,
				InputSize = 646,
				Statistics = NewTaskRunStatistics(),
			};
		}

		public static BenchmarkResultSlimDto NewBenchmarkResultSlim()
		{
			return new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.UtcNow,
				Affinity = new long[] { 1, 23 },
				Score = 465,
				TaskResults = new[] { NewTaskResultSlim() },
			};
		}

		public static ResultBatchDto NewBenchmarkResultsBatch(long? id = null)
		{
			return new ResultBatchDto
			{
				Id = id ?? NewId(),
				ElpidaVersion = NewElpida(),
				System = NewSystem(),
				BenchmarkResults = new[] { NewBenchmarkResultSlim(), NewBenchmarkResultSlim() },
			};
		}

		public static long NewId()
		{
			return new Random().Next(1, 50);
		}
	}
}