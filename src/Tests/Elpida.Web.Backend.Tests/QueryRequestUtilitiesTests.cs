// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System;
using NUnit.Framework;

namespace Elpida.Web.Backend.Tests
{
	[TestFixture]
	public class QueryRequestUtilitiesTests
	{
		private (string name, string operation) Parse(string param)
		{
			if (string.IsNullOrWhiteSpace(param))
			{
				throw new Exception("param cannot be empty");
			}

			var dot = false;
			string? name = null;
			var dotIndex = 0;
			for (var i = 0; i < param.Length; ++i)
			{
				var c = param[i];
				if (c == '.')
				{
					if (dot)
					{
						throw new Exception("parameter cannot have more than 1 dot");
					}

					if (i == param.Length - 1)
					{
						throw new Exception("parameter cannot end with dot");
					}

					if (i > 0)
					{
						name = param[..i];
					}
					else
					{
						throw new Exception("parameter cannot start with dot");
					}

					dotIndex = i + 1;
					dot = true;
					continue;
				}

				if (!char.IsLetterOrDigit(c))
				{
					throw new Exception("parameter cannot have a non letter or digit character");
				}
			}

			if (!dot)
			{
				name = param;
				return (name, "equals");
			}

			return (name, param.Substring(dotIndex));
		}

		[Test]
		[TestCase("LOL.")]
		[TestCase(".LOL.")]
		[TestCase(".LOL")]
		[TestCase(".")]
		[TestCase("..")]
		[TestCase("")]
		[TestCase(" ")]
		[TestCase(" \t\n")]
		[TestCase("lol.haha.wtf")]
		public void Foo_Exception(string input)
		{
			Assert.Throws<Exception>(() => Parse(input));
		}

		[Test]
		[TestCase("field", "field", "equals")]
		[TestCase("field.contains", "field", "contains")]
		public void Foo_Success(string input, string expectedName, string expectedOperation)
		{
			var (name, operation) = Parse(input);

			Assert.AreEqual(expectedName, name);
			Assert.AreEqual(expectedOperation, operation);
		}
	}
}