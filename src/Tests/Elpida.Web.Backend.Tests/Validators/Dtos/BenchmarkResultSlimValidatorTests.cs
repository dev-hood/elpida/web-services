// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System;
using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Result.Batch;
using NUnit.Framework;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	[TestFixture]
	internal class BenchmarkResultSlimValidatorTests : ValidatorTest<BenchmarkResultSlimDto>
	{
		protected override IEnumerable<(BenchmarkResultSlimDto, string)> GetInvalidData()
		{
			yield return (new BenchmarkResultSlimDto
			{
				Uuid = default,
				Timestamp = DateTime.Now,
				Affinity = new long[] { 1, 2, 3 },
				Score = 5,
				TaskResults = new TaskResultSlimDto[] { null! },
			}, $"default {nameof(BenchmarkResultSlimDto.Uuid)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = default,
				Affinity = new long[] { 1, 2, 3 },
				Score = 5,
				TaskResults = new TaskResultSlimDto[] { null! },
			}, $"default {nameof(BenchmarkResultSlimDto.Timestamp)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.Now,
				Affinity = default!,
				Score = 5,
				TaskResults = new TaskResultSlimDto[] { null! },
			}, $"default {nameof(BenchmarkResultSlimDto.Affinity)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.Now,
				Affinity = Array.Empty<long>(),
				Score = 5,
				TaskResults = new TaskResultSlimDto[] { null! },
			}, $"empty {nameof(BenchmarkResultSlimDto.Affinity)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.Now,
				Affinity = new long[] { 1, 2, 3 },
				Score = 5,
				TaskResults = Array.Empty<TaskResultSlimDto>(),
			}, $"empty {nameof(BenchmarkResultSlimDto.TaskResults)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.Now,
				Affinity = new long[] { 1, 2, 3 },
				Score = 5,
				TaskResults = default!,
			}, $"default {nameof(BenchmarkResultSlimDto.TaskResults)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.Now,
				Affinity = new long[] { 1, 2, 3 },
				Score = -5,
				TaskResults = new TaskResultSlimDto[] { null! },
			}, $"negative {nameof(BenchmarkResultSlimDto.Score)}");

			yield return (new BenchmarkResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Timestamp = DateTime.Now,
				Affinity = new long[] { 1, 2, 3 },
				Score = 0,
				TaskResults = new TaskResultSlimDto[] { null! },
			}, $"zero {nameof(BenchmarkResultSlimDto.Score)}");

			// yield return (new BenchmarkResultSlimDto
			// {
			// 	Uuid = Guid.NewGuid(),
			// 	Timestamp = DateTime.Now,
			// 	Affinity = new long[] { 1, -2, 3 },
			// 	Score = 5,
			// 	TaskResults = new TaskResultSlimDto[] { null! },
			// }, $"negative {nameof(BenchmarkResultSlimDto.Affinity)}");
		}
	}
}