// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Cpu;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	internal class CpuCacheDtoValidatorTests : ValidatorTest<CpuCacheDto>
	{
		protected override IEnumerable<(CpuCacheDto, string)> GetInvalidData()
		{
			yield return (new CpuCacheDto
				{
					Name = null!,
					Associativity = "Test",
					Size = 456,
					LineSize = 465,
				},
				$"null {nameof(CpuCacheDto.Name)}");

			yield return (new CpuCacheDto
				{
					Name = string.Empty,
					Associativity = "Test",
					Size = 456,
					LineSize = 465,
				},
				$"empty {nameof(CpuCacheDto.Name)}");

			yield return (new CpuCacheDto
				{
					Name = "  ",
					Associativity = "Test",
					Size = 456,
					LineSize = 465,
				},
				$"empty {nameof(CpuCacheDto.Name)}");

			yield return (new CpuCacheDto
				{
					Name = new string('A', 80),
					Associativity = "Test",
					Size = 456,
					LineSize = 465,
				},
				$"very large {nameof(CpuCacheDto.Name)}");

			yield return (new CpuCacheDto
				{
					Name = "Test",
					Associativity = new string('A', 80),
					Size = 456,
					LineSize = 465,
				},
				$"very large {nameof(CpuCacheDto.Associativity)}");

			yield return (new CpuCacheDto
			{
				Name = "Test",
				Associativity = "Test",
				Size = -456,
				LineSize = 465,
			}, $"negative {nameof(CpuCacheDto.Size)}");

			yield return (new CpuCacheDto
			{
				Name = "Test",
				Associativity = "Test",
				Size = 456,
				LineSize = -465,
			}, $"negative {nameof(CpuCacheDto.LineSize)}");
		}
	}
}