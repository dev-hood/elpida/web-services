// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Cpu;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	internal class CpuDtoValidatorTests : ValidatorTest<CpuDto>
	{
		protected override IEnumerable<(CpuDto, string)> GetInvalidData()
		{
			yield return (new CpuDto
			{
				Id = 0,
				Architecture = null!,
				Vendor = "Test",
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"null {nameof(CpuDto.Architecture)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = string.Empty,
				Vendor = "Test",
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"empty {nameof(CpuDto.Architecture)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "  ",
				Vendor = "Test",
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"empty {nameof(CpuDto.Architecture)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = new string('A', 50),
				Vendor = "Test",
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"very large {nameof(CpuDto.Architecture)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = null!,
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"null {nameof(CpuDto.Vendor)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = string.Empty,
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"empty {nameof(CpuDto.Vendor)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = "  ",
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"empty {nameof(CpuDto.Vendor)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = new string('A', 80),
				ModelName = "Test",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"very large {nameof(CpuDto.Vendor)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = "Test",
				ModelName = null!,
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"null {nameof(CpuDto.ModelName)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = "Test",
				ModelName = string.Empty,
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"empty {nameof(CpuDto.ModelName)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = "Test",
				ModelName = "   ",
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"empty {nameof(CpuDto.ModelName)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = "Test",
				ModelName = new string('A', 80),
				Frequency = 546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"very large {nameof(CpuDto.ModelName)}");

			yield return (new CpuDto
			{
				Id = 0,
				Architecture = "Test",
				Vendor = "Test",
				ModelName = "Test",
				Frequency = -546,
				Smt = true,
				Caches = null,
				Features = null,
				AdditionalInfo = null,
			}, $"negative {nameof(CpuDto.Frequency)}");
		}
	}
}