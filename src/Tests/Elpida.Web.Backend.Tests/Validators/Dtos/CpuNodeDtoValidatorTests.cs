// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Topology;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	internal class CpuNodeDtoValidatorTests : ValidatorTest<CpuNodeDto>
	{
		protected override IEnumerable<(CpuNodeDto, string)> GetInvalidData()
		{
			yield return (new CpuNodeDto
				{
					NodeType = (ProcessorNodeType)(-5),
					Name = "Test",
					Value = 5,
					OsIndex = 1,
					Children = null,
					MemoryChildren = null,
				},
				$"negative {nameof(CpuNodeDto.NodeType)}");

			yield return (new CpuNodeDto
				{
					NodeType = (ProcessorNodeType)5000,
					Name = "Test",
					Value = 5,
					OsIndex = 1,
					Children = null,
					MemoryChildren = null,
				},
				$"invalid {nameof(CpuNodeDto.NodeType)}");

			yield return (new CpuNodeDto
				{
					NodeType = ProcessorNodeType.Die,
					Name = null!,
					Value = 5,
					OsIndex = 1,
					Children = null,
					MemoryChildren = null,
				},
				$"null {nameof(CpuNodeDto.Name)}");

			yield return (new CpuNodeDto
				{
					NodeType = ProcessorNodeType.Die,
					Name = string.Empty,
					Value = 5,
					OsIndex = 1,
					Children = null,
					MemoryChildren = null,
				},
				$"empty {nameof(CpuNodeDto.Name)}");

			yield return (new CpuNodeDto
				{
					NodeType = ProcessorNodeType.Die,
					Name = "  ",
					Value = 5,
					OsIndex = 1,
					Children = null,
					MemoryChildren = null,
				},
				$"empty {nameof(CpuNodeDto.Name)}");

			yield return (new CpuNodeDto
				{
					NodeType = ProcessorNodeType.Die,
					Name = new string('A', 100),
					Value = 5,
					OsIndex = 1,
					Children = null,
					MemoryChildren = null,
				},
				$"very large {nameof(CpuNodeDto.Name)}");
		}
	}
}