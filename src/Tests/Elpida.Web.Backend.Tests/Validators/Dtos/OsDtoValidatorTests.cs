// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Os;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	internal class OsDtoValidatorTests : ValidatorTest<OperatingSystemDto>
	{
		protected override IEnumerable<(OperatingSystemDto, string)> GetInvalidData()
		{
			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = null!,
				Name = "Test",
				Version = "Test",
			}, $"null {nameof(OperatingSystemDto.Category)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = string.Empty,
				Name = "Test",
				Version = "Test",
			}, $"empty {nameof(OperatingSystemDto.Category)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "  ",
				Name = "Test",
				Version = "Test",
			}, $"empty {nameof(OperatingSystemDto.Category)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = new string('A', 100),
				Name = "Test",
				Version = "Test",
			}, $"very large {nameof(OperatingSystemDto.Category)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = null!,
				Version = "Test",
			}, $"null {nameof(OperatingSystemDto.Name)}");

			yield return (new OperatingSystemDto
				{
					Id = 0,
					Category = "Test",
					Name = string.Empty,
					Version = "Test",
				},
				$"empty {nameof(OperatingSystemDto.Name)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = "  ",
				Version = "Test",
			}, $"empty {nameof(OperatingSystemDto.Name)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = new string('A', 200),
				Version = "Test",
			}, $"very large {nameof(OperatingSystemDto.Name)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = "Test",
				Version = null!,
			}, $"null {nameof(OperatingSystemDto.Version)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = "Test",
				Version = string.Empty,
			}, $"empty {nameof(OperatingSystemDto.Version)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = "Test",
				Version = "  ",
			}, $"empty {nameof(OperatingSystemDto.Version)}");

			yield return (new OperatingSystemDto
			{
				Id = 0,
				Category = "Test",
				Name = "Test",
				Version = new string('A', 100),
			}, $"very large {nameof(OperatingSystemDto.Version)}");
		}
	}
}