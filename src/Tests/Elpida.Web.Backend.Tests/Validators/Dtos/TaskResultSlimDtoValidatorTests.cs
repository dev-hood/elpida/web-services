// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System;
using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Result;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Result.Batch;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	internal class TaskResultSlimDtoValidatorTests : ValidatorTest<TaskResultSlimDto>
	{
		protected override IEnumerable<(TaskResultSlimDto, string)> GetInvalidData()
		{
			yield return (new TaskResultSlimDto
			{
				Uuid = Guid.Empty,
				Value = 51,
				Time = 156,
				InputSize = 654,
				Statistics = new TaskRunStatisticsDto
				{
					Max = 54,
					Min = 654,
					Mean = 13,
					Tau = 64,
					SampleSize = 89,
					MarginOfError = 64,
					StandardDeviation = 12,
				},
			}, $"empty {nameof(TaskResultSlimDto.Uuid)}");

			yield return (new TaskResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Value = 0,
				Time = 156,
				InputSize = 654,
				Statistics = new TaskRunStatisticsDto
				{
					Max = 54,
					Min = 654,
					Mean = 13,
					Tau = 64,
					SampleSize = 89,
					MarginOfError = 64,
					StandardDeviation = 12,
				},
			}, $"zero {nameof(TaskResultSlimDto.Value)}");

			yield return (new TaskResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Value = -51,
				Time = 156,
				InputSize = 654,
				Statistics = new TaskRunStatisticsDto
				{
					Max = 54,
					Min = 654,
					Mean = 13,
					Tau = 64,
					SampleSize = 89,
					MarginOfError = 64,
					StandardDeviation = 12,
				},
			}, $"negative {nameof(TaskResultSlimDto.Value)}");

			yield return (new TaskResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Value = 51,
				Time = 0,
				InputSize = 654,
				Statistics = new TaskRunStatisticsDto
				{
					Max = 54,
					Min = 654,
					Mean = 13,
					Tau = 64,
					SampleSize = 89,
					MarginOfError = 64,
					StandardDeviation = 12,
				},
			}, $"zero {nameof(TaskResultSlimDto.Time)}");

			yield return (new TaskResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Value = 51,
				Time = -156,
				InputSize = 654,
				Statistics = new TaskRunStatisticsDto
				{
					Max = 54,
					Min = 654,
					Mean = 13,
					Tau = 64,
					SampleSize = 89,
					MarginOfError = 64,
					StandardDeviation = 12,
				},
			}, $"negative {nameof(TaskResultSlimDto.Time)}");

			yield return (new TaskResultSlimDto
			{
				Uuid = Guid.NewGuid(),
				Value = 51,
				Time = 156,
				InputSize = 654,
				Statistics = null!,
			}, $"null {nameof(TaskResultSlimDto.Statistics)}");
		}
	}
}