// =========================================================================
//
// Elpida HTTP Rest API
//
// Copyright (C) 2021 Ioannis Panagiotopoulos
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// =========================================================================

using System.Collections.Generic;
using Elpida.Web.Backend.Services.Abstractions.Dtos.Result;

namespace Elpida.Web.Backend.Tests.Validators.Dtos
{
	internal class TaskRunStatisticsDtoValidatorTests : ValidatorTest<TaskRunStatisticsDto>
	{
		protected override IEnumerable<(TaskRunStatisticsDto, string)> GetInvalidData()
		{
			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = 31,
					Min = 654,
					Mean = 46,
					StandardDeviation = 13,
					Tau = 12,
					MarginOfError = 97,
				},
				$"zero {nameof(TaskRunStatisticsDto.SampleSize)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = -5,
					Max = 31,
					Min = 654,
					Mean = 46,
					StandardDeviation = 13,
					Tau = 12,
					MarginOfError = 97,
				},
				$"negative {nameof(TaskRunStatisticsDto.SampleSize)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = -31,
					Min = 654,
					Mean = 46,
					StandardDeviation = 13,
					Tau = 12,
					MarginOfError = 97,
				},
				$"negative {nameof(TaskRunStatisticsDto.Max)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = 31,
					Min = -654,
					Mean = 46,
					StandardDeviation = 13,
					Tau = 12,
					MarginOfError = 97,
				},
				$"negative {nameof(TaskRunStatisticsDto.Min)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = 31,
					Min = 654,
					Mean = -46,
					StandardDeviation = 13,
					Tau = 12,
					MarginOfError = 97,
				},
				$"negative {nameof(TaskRunStatisticsDto.Mean)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = 31,
					Min = 654,
					Mean = 46,
					StandardDeviation = -13,
					Tau = 12,
					MarginOfError = 97,
				},
				$"negative {nameof(TaskRunStatisticsDto.StandardDeviation)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = 31,
					Min = 654,
					Mean = 46,
					StandardDeviation = 13,
					Tau = -12,
					MarginOfError = 97,
				},
				$"negative {nameof(TaskRunStatisticsDto.Tau)}");

			yield return (new TaskRunStatisticsDto
				{
					SampleSize = 0,
					Max = 31,
					Min = 654,
					Mean = 46,
					StandardDeviation = 13,
					Tau = 12,
					MarginOfError = -97,
				},
				$"negative {nameof(TaskRunStatisticsDto.MarginOfError)}");
		}
	}
}